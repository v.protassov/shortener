# shortener

Сокращает ссылки, по которым потом можно перейти
 
Для работы сделать `docker-compose up` и перейти на `localhost`


**Задания 1, 2**

```python
def draw_rating(vote):
	if vote >= 0 && vote <= 20: # Оператор И в питоне обозначается, как and
    	return '★☆☆☆☆'
	elif vote > 20 && vote <= 40: # лучше использовать двойной оператор сравнения
		return '★★☆☆☆'
	elif vote > 40 && vote <= 60:
		return '★★★☆☆'
	elif vote > 60 && vote <= 80:
		return '★★★★☆'
	elif vote > 80 && vote <= 100:
		return '★★★★★'
```

в итоге, код должен выглядеть так:

```python
def draw_rating(vote):
    if 0 <= vote <= 20:
        return '★☆☆☆☆'
    elif 20 < vote <= 40:
        return '★★☆☆☆'
    elif 40 < vote <= 60:
        return '★★★☆☆'
    elif 60 < vote <= 80:
        return '★★★★☆'
    elif 80 < vote <= 100:
        return '★★★★★'
```

```python
def func(s, a, b):
    if re.compile('/^$/').match(s):  # символ ^ означает начало строки, $ - конец строки, а тут оба символа стоят посреди строки
        return -1

    i = len(s) - 1
    a_index = -1
    b_index = -1

    while (a_index == -1) or (b_index == -1) or (i > 0):  # много сравнений, сложно читается, я бы заменил на any()
        if s[i:i + 1] == a:  # одинаковые действия здесь и через строку, надо вынести в отдельную переменную
            a_index = i
        if s[i:i + 1] == b:
            b_index = i
        i = i - 1 # -= 1

    if a_index != -1:
        if b_index == -1:
            return a_index
        else:
            return max([a_index, b_index])

    if b_index != -1:
        return b_index
    else:
        return -1
```

В итоге код должен выглядеть так:
```python
def func(s, a, b):
    if re.compile('/^$/').match(s): 
        return -1

    i = len(s) - 1
    a_index = -1
    b_index = -1

    while any ([(a_index == -1), (b_index == -1), (i > 0)]):
        var = s[i:i + 1]
        if var == a: 
            a_index = i
        if var == b:
            b_index = i
        i -= 1 

    if a_index != -1:
        if b_index == -1:
            return a_index
        else:
            return max([a_index, b_index])

    if b_index != -1:
        return b_index
    else:
        return -1
```
