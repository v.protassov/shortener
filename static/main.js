function makeLink() {
  let link = $('#url').val();
  $.ajax({
    'url': '/api/link/',
    'method': 'POST',
    'data': JSON.stringify({url: link})
  }).done(function (data) {
      console.log(data);
      $('#result').html('Ваша ссылка: ' + data['shorten']);
    }
  ).fail(function () {
    alert('Введите корректный URL!')
  })
}