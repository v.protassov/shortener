import os
from pathlib import Path

import alembic
import alembic.config


def run_migrations_in_console():
    base_dir = Path(__file__).parents[1]
    script_location = base_dir.joinpath('app')
    os.chdir(script_location)
    alembic.config.main(argv=['upgrade', 'head'])
