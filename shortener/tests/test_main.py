from models import Link


async def test_obtain_link_should_create_link(test_cli):
    request = await test_cli.post('/api/link/', json={'url': 'https://yandex.ru'})
    request_json = await request.json()

    link = await Link.get(request_json['id'])

    assert link.url == 'https://yandex.ru'


async def test_obtain_link_should_return_error_if_link_is_incorrect(test_cli):
    request = await test_cli.post('/api/link/', json={'url': 'yandex.ru'})

    assert request.status == 400


async def test_obtain_link_should_return_shorten_url(test_cli):
    request = await test_cli.post('/api/link/', json={'url': 'https://yandex.ru'})
    request_json = await request.json()

    shorten_url = request_json['shorten']

    assert shorten_url is not None


async def test_redirect_should_redirect_to_needed_site(test_cli, session):
    request = await test_cli.post('/api/link/', json={'url': 'https://yandex.ru'})
    request_json = await request.json()

    shorten_url = request_json['shorten']

    request2 = await session.get(shorten_url)
    assert request2.host == 'yandex.ru'
