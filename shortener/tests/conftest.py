import datetime
import os
import socket

import aiohttp
import pytest
import sqlalchemy
from aiohttp import ClientSession
from sqlalchemy_utils import database_exists, create_database, drop_database

import main
import models
from tests.tests_helpers import run_migrations_in_console

os.environ['APP_ENV'] = 'TEST'

from dateutil import tz

from app import settings
from app.models import db

ECHO = False

PG_URL = settings.CONNECTION_STRING
FAKE_TIME = datetime.datetime(2020, 12, 25, 17, 5, 55, tzinfo=tz.UTC)


def pytest_sessionstart(session):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    engine = sqlalchemy.create_engine(settings.CONNECTION_STRING)
    if database_exists(engine.url):
        drop_database(engine.url)
    create_database(engine.url)
    run_migrations_in_console()


def pytest_sessionfinish(session, exitstatus):
    """
    Called after whole test run finished, right before
    returning the exit status to the system.
    """
    engine = sqlalchemy.create_engine(settings.CONNECTION_STRING)
    drop_database(engine.url)


@pytest.fixture(scope='module')
def sa_engine():
    rv = sqlalchemy.create_engine(PG_URL, echo=ECHO)
    db.create_all(rv)
    yield rv
    db.drop_all(rv)
    rv.dispose()


@pytest.yield_fixture
def app():
    _app = main.app
    yield _app


@pytest.fixture
def test_cli(loop, app, sanic_client):
    return loop.run_until_complete(sanic_client(app))


@pytest.fixture
async def database():
    return await db.set_bind(settings.CONNECTION_STRING)


@pytest.fixture
async def clean_db():
    yield
    await models.Link.delete.gino.status()


@pytest.fixture
async def session():
    connector = aiohttp.TCPConnector(
        family=socket.AF_INET,
        ssl=False
    )
    async with ClientSession(connector=connector) as session:
        yield session

