#!/usr/bin/env bash

export PYTHONPATH="/opt/src/app/:/opt/src/"

host="$1"
dbname="$2"
user="$3"
password="$4"

create_database_sql="create database $dbname"

sleep 5

create_db_cmd="psql -h $host -U $user -t -c \"$create_database_sql\" "
eval "$create_db_cmd"

select_count_sql="select count(1) from pg_catalog.pg_database where datname = '$dbname'"
create_database_sql="create database $dbname"
cmd="psql -h $host -U $user -t -c \"$select_count_sql\""
sleep 15
db_exists=$(eval "$cmd")
if [ "$db_exists" -eq 0 ] ; then

   cmd="psql -h $host -U $user -t -c \"$create_database_sql\" > /dev/null 2>&1"
   eval "$cmd"
   sleep 3
fi
cd /opt/src/shortener/app || exit
alembic upgrade head
pytest ../tests/
python3 /opt/src/shortener/app/main.py
