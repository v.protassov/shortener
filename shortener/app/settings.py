import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
HTML_DIR = BASE_DIR + '/frontend/html'

env = os.environ.get('APP_ENV')

PG_USERNAME = 'postgres'
PG_PASSWORD = 'postgres'
PG_HOST = 'shortener-db'
PG_PORT = '5432'
PG_DATABASE = 'shortener'
if env == 'TEST':
    PG_DATABASE = 'shortener-test'
if env not in ('LOCAL', 'TEST'):
    PG_HOST = '127.0.0.1'

CONNECTION_STRING = 'postgresql://{user}:{password}@{host}:{port}/{db}'.format(user=PG_USERNAME,
                                                                               password=PG_PASSWORD,
                                                                               host=PG_HOST, port=PG_PORT,
                                                                               db=PG_DATABASE)

SLEEP_TIME = 2
