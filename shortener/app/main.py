from pydantic import ValidationError
from sanic import Sanic, response
from sanic.request import Request
from sanic.response import json, HTTPResponse

import serializers
import settings
from helpers import encode, decode
from models import db

from models import Link


def create_app():
    app_ = Sanic()
    app_.config.DB_HOST = settings.PG_HOST
    app_.config.DB_PORT = settings.PG_PORT
    app_.config.DB_USER = settings.PG_USERNAME
    app_.config.DB_PASSWORD = settings.PG_PASSWORD
    app_.config.DB_DATABASE = settings.PG_DATABASE
    db.init_app(app_)
    return app_


app = create_app()


@app.route('/')
async def index(request: Request):
    with open(settings.HTML_DIR + '/index.html', 'r') as fl:
        return response.html(fl.read())


@app.route('/api/link/', methods=('POST',))
async def obtain_link(request: Request) -> HTTPResponse:
    try:
        req = request.json
        serializers.LinkSerializer(**req)
        created_link = await Link.create(**req)
        result = serializers.LinkSerializer.from_orm(created_link)
        result.shorten = "{}://{}/{}".format(request.scheme, request.host, encode(result.id))
        return json(result.dict())
    except ValidationError as e:
        return json(e.errors(), status=400)


@app.route('/<shorten:string>')
async def redirect(request: Request, shorten: str) -> HTTPResponse:
    if shorten is None:
        return HTTPResponse(status=404)
    link_id = decode(shorten)
    link = await Link.get(link_id)
    if link is None:
        return HTTPResponse(status=404)
    return response.redirect(link.url)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
