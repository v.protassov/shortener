from gino.ext.sanic import Gino
from sqlalchemy import func

db = Gino()


class Link(db.Model):
    __tablename__ = 'links'

    id = db.Column(db.Integer(), primary_key=True)
    url = db.Column(db.String(3000))
    created = db.Column(db.DateTime(timezone=True), server_default=func.now())
