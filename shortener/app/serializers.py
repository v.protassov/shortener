from datetime import datetime

import validators
from pydantic import BaseModel, Field, validator


class LinkSerializer(BaseModel):
    id: int = Field(None, readonly=True)
    url: str
    created: datetime = Field(None, readonly=True)
    shorten: int = Field(None, readonly=True)

    @validator('url')
    def must_be_an_url(cls, v):
        if not validators.url(v):
            raise ValueError('Valid url should be used')
        return v

    class Config:
        orm_mode = True
